<?php

use App\App;

$conn = App::DB()->getConnection();

$conn->query(<<< SQL
    CREATE TABLE IF NOT EXISTS shop (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL,
    type VARCHAR(16) NOT NULL);
    SQL);

$conn->query(<<< SQL
    CREATE TABLE IF NOT EXISTS product (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL,
    type VARCHAR(16) NOT NULL);
    SQL);

$conn->query(<<< SQL
    CREATE TABLE IF NOT EXISTS bill (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    shop_id INTEGER NOT NULL,
    number INTEGER NOT NULL,
    craeted_at INTEGER NOT NULL,
    customer_first_name VARCHAR(32) NOT NULL,
    customer_last_name VARCHAR(32) NOT NULL,
    customer_phone VARCHAR(16) NOT NULL);
    SQL);

$conn->query(<<< SQL
    CREATE TABLE IF NOT EXISTS shop_product (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    shop_id INTEGER NOT NULL,
    product_id INTEGER NOT NULL,
    price FLOAT NOT NULL,
    amount INTEGER NOT NULL);
    SQL);

$conn->query(<<< SQL
    CREATE TABLE IF NOT EXISTS bill_product (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    bill_id INTEGER NOT NULL,
    product_id INTEGER NOT NULL,
    price FLOAT NOT NULL,
    amount INTEGER NOT NULL,
    serial_number VARCHAR(32) DEFAULT NULL);
    SQL);
