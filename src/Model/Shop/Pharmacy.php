<?php
namespace App\Model\Shop;

use App\Model\Product\Product;

class Pharmacy extends AbstractShop
{
    public const TYPE_PHARMACY = 'pharmacy';

    protected static array $disallowedProductTypes = [
        Product::TYPE_PARKING_TICKETS,
    ];

    public function getType(): string
    {
        return self::TYPE_PHARMACY;
    }
}
