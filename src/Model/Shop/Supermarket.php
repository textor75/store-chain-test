<?php
namespace App\Model\Shop;

use App\Model\Product\Product;

class Supermarket extends AbstractShop
{
    public const TYPE_SUPERMARKET = 'supermarket';

    protected static array $disallowedProductTypes = [
        Product::TYPE_MEDICINE,
        Product::TYPE_PARKING_TICKETS,
    ];

    public function getType(): string
    {
        return self::TYPE_SUPERMARKET;
    }
}
