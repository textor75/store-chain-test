<?php
namespace App\Model\Shop;

use App\Model;
use App\Model\Product\Product;

abstract class AbstractShop extends Model
{
    protected static array $disallowedProductTypes = [];

    protected string $name;

    // corner shop, supermarket and pharmacy
    abstract public function getType(): string;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function canNotSaleProduct(Product $product): bool
    {
        return in_array($product->getType(), static::$disallowedProductTypes, true);
    }
}
