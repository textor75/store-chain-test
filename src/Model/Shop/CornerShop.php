<?php
namespace App\Model\Shop;

use App\Model\Product\Product;

class CornerShop extends AbstractShop
{
    public const TYPE_CORNER_SHOP = 'corner_shop';

    protected static array $disallowedProductTypes = [
        Product::TYPE_MEDICINE,
    ];

    public function getType(): string
    {
        return self::TYPE_CORNER_SHOP;
    }
}
