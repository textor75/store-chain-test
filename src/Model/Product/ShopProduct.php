<?php
namespace App\Model\Product;

use App\Model;
use App\Model\Shop\AbstractShop;

class ShopProduct extends Model
{
    private AbstractShop $shop;
    private Product $product;
    private float $price;
    private int $amount;

    public function getShop(): AbstractShop
    {
        return $this->shop;
    }

    public function setShop(AbstractShop $shop): void
    {
        $this->shop = $shop;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    public function needSerialNumber(): bool
    {
        return $this->product->needSerialNumber();
    }

    public function decreaseAmount(int $amount): void
    {
        $this->amount -= $amount;
    }
}
