<?php
namespace App\Model\Product;

use App\Model;
use Exception;

class Product extends Model
{
    public const TYPE_FOOD = 'food';
    public const TYPE_DRINK = 'drink';
    public const TYPE_MEDICINE = 'medicine';
    public const TYPE_CIGARETTES = 'cigarettes';
    public const TYPE_TOYS = 'toys';
    public const TYPE_PARKING_TICKETS = 'parking tickets';

    private const TYPES = [
        self::TYPE_FOOD,
        self::TYPE_DRINK,
        self::TYPE_MEDICINE,
        self::TYPE_CIGARETTES,
        self::TYPE_TOYS,
        self::TYPE_PARKING_TICKETS,
    ];

    private const NEED_SERIAL_NUMBER = [
        self::TYPE_MEDICINE,
        self::TYPE_PARKING_TICKETS,
    ];

    private string $type;
    private string $name;

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        if (!in_array($type, self::TYPES)) {
            throw new Exception('Unknown product type: ' . $type);
        }

        $this->type = $type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function needSerialNumber(): bool
    {
        return in_array($this->type, self::NEED_SERIAL_NUMBER, true);
    }
}
