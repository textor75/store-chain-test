<?php
namespace App\Model\Bill;

use App\Model;
use App\Model\Shop\AbstractShop;
use DateTimeInterface;

class Bill extends Model
{
    private AbstractShop $shop;
    private int $number;
    private DateTimeInterface $createdAt;
    private string $customerFirstName;
    private string $customerLastName;
    private string $customerPhone;

    public function getShop(): AbstractShop
    {
        return $this->shop;
    }

    public function setShop(AbstractShop $shop): void
    {
        $this->shop = $shop;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber(int $number): void
    {
        $this->number = $number;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getCustomerFirstName(): string
    {
        return $this->customerFirstName;
    }

    public function getCustomerLastName(): string
    {
        return $this->customerLastName;
    }

    public function getCustomerPhone(): string
    {
        return $this->customerPhone;
    }

    public function setCustomer(string $firstName, string $lastName, string $phone): void
    {
        $this->customerFirstName = $firstName;
        $this->customerLastName = $lastName;
        $this->customerPhone = $phone;
    }
}