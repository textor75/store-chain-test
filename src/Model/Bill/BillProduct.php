<?php
namespace App\Model\Bill;

use App\Model;
use App\Model\Product\Product;

class BillProduct extends Model
{
    private Bill $bill;
    private Product $product;
    private int $amount;
    private int $price;
    private ?string $serialNumber;

    public function getBill(): Bill
    {
        return $this->bill;
    }

    public function setBill(Bill $bill): void
    {
        $this->bill = $bill;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(?string $serialNumber): void
    {
        $this->serialNumber = $serialNumber;
    }
}
