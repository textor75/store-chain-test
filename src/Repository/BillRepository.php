<?php
namespace App\Repository;

use DateTime;

class BillRepository extends AbstractRepository
{
    public const TABLE_BILL = 'bill';

    public function getTable(): string
    {
        return self::TABLE_BILL;
    }

    public function findMaxNumber(int $shopId, int $year): int
    {
        $sql = sprintf(
            'SELECT MAX(number) AS max_number FROM bill WHERE shop_id = %d AND created_at BETWEEN %d AND %d',
            $shopId,
            (new DateTime($year . '-01-01 00:00:00' ))->getTimestamp(),
            (new DateTime($year . '-12-31 23:59:59' ))->getTimestamp()
        );

        $result = $this->execute($sql)->fetchArray();
        if (!$result) {
            return 0;
        }

        return (int) reset($result)['max_number'];
    }
}
