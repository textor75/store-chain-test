<?php
namespace App\Repository;

use App\App;
use SQLite3Result;

abstract class AbstractRepository
{
    abstract public function getTable(): string;

    public function find(int $id): ?array
    {
        $sql = sprintf('SELECT * FROM %s WHERE id = %d LIMIT 1', $this->getTable(), $id);
        $result = $this->execute($sql)->fetchArray();

        return $result ? reset($result) : null;
    }

    public function execute($sql): SQLite3Result
    {
        return App::DB()->getConnection()->query($sql);
    }
}
