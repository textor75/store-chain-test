<?php
namespace App\Repository;

class ShopProductRepository extends AbstractRepository
{
    public const TABLE_SHOP_PRODUCT = 'shop_product';

    public function getTable(): string
    {
        return self::TABLE_SHOP_PRODUCT;
    }
}
