<?php
namespace App\Repository;

use App\Model\Shop\AbstractShop;
use DateTimeInterface;

class BillProductRepository extends AbstractRepository
{
    public const TABLE_BILL_PRODUCT = 'bill_product';

    public function getTable(): string
    {
        return self::TABLE_BILL_PRODUCT;
    }

    public function findByShopInPeriod(AbstractShop $shop, DateTimeInterface $from, DateTimeInterface $to): array
    {
        $sql = sprintf(
            'SELECT bp.* FROM bill_product bp INNER JOIN bill b ON bp.bill_id = b.id WHERE b.shop_id = %s AND b.created_at BETWEEN %d AND %d',
            $shop->getId(),
            $from->getTimestamp(),
            $to->getTimestamp(),
        );

        return $this->execute($sql)->fetchArray();
    }
}
