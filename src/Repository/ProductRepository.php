<?php
namespace App\Repository;

class ProductRepository extends AbstractRepository
{
    public const TABLE_PRODUCT = 'product';

    public function getTable(): string
    {
        return self::TABLE_PRODUCT;
    }
}
