<?php
namespace App\Repository;

class ShopRepository extends AbstractRepository
{
    public const TABLE_SHOP = 'shop';

    public function getTable(): string
    {
        return self::TABLE_SHOP;
    }
}
