<?php
namespace App\DTO;

use App\Model\Product\ShopProduct;

class SaleDTO
{
    public ShopProduct $shopProduct;
    public int $amount;
    public ?string $serialNumber;
}
