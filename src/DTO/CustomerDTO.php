<?php
namespace App\DTO;

class CustomerDTO
{
    public string $firstName;
    public string $lastName;
    public string $phone;
}