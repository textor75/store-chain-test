<?php
namespace App;

use SQLite3;

class DB
{
    private SQLite3 $connection;

    public function __construct()
    {
        $path = App::Config()->get('db', 'path');
        $this->connection = new SQLite3($path);
    }

    public function __destruct()
    {
        $this->connection->close();
    }

    public function getConnection(): SQLite3
    {
        return $this->connection;
    }


}
