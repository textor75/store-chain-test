<?php
namespace App\Service;

use App\Model\Shop\AbstractShop;
use App\Repository\BillRepository;
use DateTimeInterface;

class BillNumberGenerator
{
    public function generateNumber(AbstractShop $shop, DateTimeInterface $date): string
    {
        // todo: lock or reserve generated number
        $maxNumber = (new BillRepository())->findMaxNumber($shop->getId(), (int) $date->format('Y'));
        $maxNumber++;
        return $maxNumber;
    }
}
