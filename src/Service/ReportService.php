<?php
namespace App\Service;

use App\Model\Bill\BillProduct;
use App\Model\Shop\AbstractShop;
use App\ModelCreator\BillProductModelCreator;
use App\Repository\BillProductRepository;
use DateTimeInterface;

class ReportService
{
    /**
     * @param AbstractShop $shop
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @return BillProduct[]
     * @throws \Exception
     */
    public function createReport(AbstractShop $shop, DateTimeInterface $from, DateTimeInterface $to): array
    {
        $data = (new BillProductRepository())->findByShopInPeriod($shop, $from, $to);

        $modelCreator = new BillProductModelCreator();
        $billProducts = [];
        foreach ($data as $item) {
            $billProducts[] = $modelCreator->createFromData($item);
        }

        return $billProducts;
    }
}