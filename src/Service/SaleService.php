<?php
namespace App\Service;

use App\DTO\CustomerDTO;
use App\DTO\SaleDTO;
use App\Model\Bill\Bill;
use App\Model\Bill\BillProduct;
use App\Model\Shop\AbstractShop;
use DateTimeInterface;
use Exception;

class SaleService
{
    /**
     * @param DateTimeInterface $date
     * @param AbstractShop $shop
     * @param CustomerDTO $customer
     * @param SaleDTO[] $sales
     * @throws Exception
     */
    public function sale(DateTimeInterface $date, AbstractShop $shop, CustomerDTO $customer, array $sales): void
    {
        // todo Wrap in a transaction
        $bill = new Bill();
        $bill->setCreatedAt($date);
        $bill->setCustomer($customer->firstName, $customer->lastName, $customer->phone);
        $number = (new BillNumberGenerator())->generateNumber($shop, $date);
        $bill->setNumber($number);

        $billProducts = [];
        $shopProducts = [];
        foreach ($sales as $sale) {
            $this->validateSale($shop, $sale);
            $billProducts[] = $this->generateBillProductAndDecreaseAmount($sale);
            $shopProducts[] = $sale->shopProduct;
        }

        (new BillManager())->save($bill, $billProducts);
        (new ShopProductManager())->updateShopProductsAmount($shopProducts);
    }

    private function validateSale(AbstractShop $shop, SaleDTO $sale): void
    {
        $shopProduct = $sale->shopProduct;
        $amount = $sale->amount;
        if ($shopProduct->getShop()->getId() !== $shop->getId()) {
            throw new Exception('Product Shop is not equal Bill Shop');
        }

        if ($shopProduct->getAmount() < $amount) {
            throw new Exception('Product amount is not enough');
        }

        if (!$sale->serialNumber && $shopProduct->needSerialNumber()) {
            throw new Exception('Product needs a serial number');
        }

        if ($shop->canNotSaleProduct($shopProduct->getProduct()))  {
            throw new Exception('Shop can not sale product');
        }
    }

    private function generateBillProductAndDecreaseAmount(SaleDTO $sale): BillProduct
    {
        $shopProduct = $sale->shopProduct;
        $amount = $sale->amount;

        $billProduct = new BillProduct();

        $billProduct->setProduct($shopProduct->getProduct());
        $billProduct->setAmount($amount);
        $billProduct->setPrice($shopProduct->getPrice());
        $billProduct->setSerialNumber($sale->serialNumber);

        $shopProduct->decreaseAmount($amount);

        return $billProduct;
    }
}