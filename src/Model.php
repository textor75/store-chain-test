<?php
namespace App;

use Exception;

class Model
{
    protected int $id;

    public function getId(): int
    {
        return $this->id;
    }

    public function isNew(): bool
    {
        return !isset($this->id);
    }

    public function setId(int $id): void
    {
        if (!$this->isNew()) {
            throw new Exception('Set Id for not new model is disallowed');
        }

        $this->id = $id;
    }
}
