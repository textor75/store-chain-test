<?php
namespace App;

use Exception;

class Config
{
    private array $cache = [];

    /**
     * @param string $name
     * @param string $key
     * @param null $defaultValue
     * @return mixed|null
     * @throws Exception
     */
    public function get(string $name, string $key, $defaultValue = null)
    {
        if (isset($this->cache[$name][$key])) {
            return $this->cache[$name][$key];
        }

        $fileName = $_SERVER['DOCUMENT_ROOT'] . '/config/' . $name . '.php';
        if (!is_file($fileName)) {
            throw new Exception('There is not config file: ' . $fileName);
        }

        $this->cache[$name] = include $fileName;

        return isset($this->cache[$name][$key]) ? $this->cache[$name][$key] : $defaultValue;
    }
}
