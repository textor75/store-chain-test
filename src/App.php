<?php
namespace App;

class App
{
    private static DB $_DB;
    private static Config $_Config;

    public static function DB(): DB
    {
        if (!isset(self::$_DB)) {
            self::$_DB = new DB();
        }

        return self::$_DB;
    }

    public static function Config(): Config
    {
        if (!isset(self::$_Config)) {
            self::$_Config = new Config();
        }

        return self::$_Config;
    }
}
