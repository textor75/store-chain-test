<?php
namespace App\ModelCreator;

use App\Model;
use App\Model\Bill\Bill;
use App\Repository\BillRepository;
use DateTimeImmutable;
use Exception;

class BillModelCreator extends AbstractModelCreator
{
    public function getModel(int $id): ?Bill
    {
        $data = (new BillRepository())->find($id);
        if ($data === null) {
            return null;
        }

        return $this->createFromData($data);
    }

    /**
     * @param array $data
     * @return Bill
     * @throws Exception
     */
    public function createFromData(array $data): Model
    {
        $model = new Bill();

        $shop = (new ShopModelCreator())->getModel($data['shop_id']);
        $model->setShop($shop);
        $model->setNumber($data['number']);
        $model->setCreatedAt(DateTimeImmutable::createFromFormat('U', $data['created_at']));
        $model->setCustomer($data['customer_first_name'], $data['customer_last_name'], $data['customer_phone']);

        return $model;
    }
}
