<?php
namespace App\ModelCreator;

use App\Model;
use App\Repository\ShopProductRepository;
use Exception;
use App\Model\Product\ShopProduct;

class ShopProductModelCreator extends AbstractModelCreator
{
    public function getModel(int $id): ?ShopProduct
    {
        $data = (new ShopProductRepository())->find($id);
        if ($data === null) {
            return null;
        }

        return $this->createFromData($data);
    }

    /**
     * @param array $data
     * @return ShopProduct
     * @throws Exception
     */
    public function createFromData(array $data): Model
    {
        $model = new ShopProduct();

        $shop = (new ShopModelCreator())->getModel($data['shop_id']);
        $product = (new ProductModelCreator())->getModel($data['product_id']);
        $model->setShop($shop);
        $model->setProduct($product);
        $model->setAmount($data['amount']);
        $model->setPrice($data['price']);

        return $model;
    }
}
