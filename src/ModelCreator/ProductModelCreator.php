<?php
namespace App\ModelCreator;

use App\Model;
use App\Model\Product\Product;
use App\Repository\ProductRepository;
use Exception;

class ProductModelCreator extends AbstractModelCreator
{
    public function getModel(int $id): ?Product
    {
        $data = (new ProductRepository())->find($id);
        if ($data === null) {
            return null;
        }

        return $this->createFromData($data);
    }

    /**
     * @param array $data
     * @return Product
     * @throws Exception
     */
    public function createFromData(array $data): Model
    {
        $model = new Product();

        $model->setId($data['id']);
        $model->setName($data['name']);
        $model->setType($data['type']);

        return $model;
    }
}
