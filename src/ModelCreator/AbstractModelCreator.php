<?php
namespace App\ModelCreator;

use App\Model;

abstract class AbstractModelCreator
{
    abstract public function createFromData(array $data): Model;
}