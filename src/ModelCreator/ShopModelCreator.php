<?php
namespace App\ModelCreator;

use App\Model;
use App\Model\Shop\AbstractShop;
use App\Model\Shop\CornerShop;
use App\Model\Shop\Pharmacy;
use App\Model\Shop\Supermarket;
use App\Repository\ShopRepository;
use Exception;

class ShopModelCreator extends AbstractModelCreator
{
    public function getModel(int $id): ?AbstractShop
    {
        $data = (new ShopRepository())->find($id);
        if ($data === null) {
            return null;
        }

        return $this->createFromData($data);
    }

    /**
     * @param array $data
     * @return AbstractShop
     * @throws Exception
     */
    public function createFromData(array $data): Model
    {
        switch ($data['type']) {
            case CornerShop::TYPE_CORNER_SHOP:
                $model = new CornerShop();
                break;
            case Supermarket::TYPE_SUPERMARKET:
                $model = new Supermarket();
                break;
            case Pharmacy::TYPE_PHARMACY:
                $model = new Pharmacy();
                break;
            default:
                throw new Exception('Unknown type: ' . $data['type']);
        }

        $model->setId($data['id']);
        $model->setName($data['name']);

        return $model;
    }
}
