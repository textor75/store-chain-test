<?php
namespace App\ModelCreator;

use App\Model;
use App\Repository\BillProductRepository;
use Exception;
use App\Model\Bill\BillProduct;

class BillProductModelCreator extends AbstractModelCreator
{
    public function getModel(int $id): ?BillProduct
    {
        $data = (new BillProductRepository())->find($id);
        if ($data === null) {
            return null;
        }

        return $this->createFromData($data);
    }

    /**
     * @param array $data
     * @return BillProduct
     * @throws Exception
     */
    public function createFromData(array $data): Model
    {
        $model = new BillProduct();

        $bill = (new BillModelCreator())->getModel($data['bill_id']);
        $product = (new ProductModelCreator())->getModel($data['product_id']);
        $model->setBill($bill);
        $model->setProduct($product);
        $model->setAmount($data['amount']);
        $model->setPrice($data['price']);
        $model->setSerialNumber($data['serial_number']);

        return $model;
    }
}
